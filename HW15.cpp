﻿#include <iostream>

void Numbers(int Limit, bool IsOdd)
{
	for (int i = (int)IsOdd; i <= Limit; i += 2)
	{
		std::cout << i << '\n';
	}
}

int main()
{
	const int limit = 10;
	bool isOdd = true;

	Numbers(limit, isOdd);
}